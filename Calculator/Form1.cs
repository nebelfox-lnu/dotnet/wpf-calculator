using Functions;

namespace Calculator
{
    public partial class Form1 : Form
    {
        private readonly Compiler _compiler;
        private TextBox _inputField;
        private Label _resultLabel;
        private Button[,] _buttons;
        private const int _padding = 10;
        private const int _defaultHeight = 690;
        private const int _defaultWidth = 620;
        private const int _inputFieldHeight = 120;
        private const int _resultLabelHeight = 50;

        public Form1()
        {
            _compiler = new Compiler(GetGrammar());

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SuspendLayout();

            // Form
            ClientSize = new Size(_defaultWidth, _defaultHeight);
            MinimumSize = Size;
            Name = "Calculator";
            Text = "Calculator";
            StartPosition = FormStartPosition.CenterScreen;
            Resize += OnResize;

            // Input Field
            _inputField = new TextBox
            {
                Location = new Point(_padding, _padding),
                Multiline = true,
                Name = "InputField",
                Size = new Size(_defaultWidth - _padding * 2, _inputFieldHeight),
                TabIndex = 0
            };
            _inputField.MinimumSize = _inputField.Size;
            Controls.Add(_inputField);

            // Result Label
            _resultLabel = new Label
            {
                Location = new Point(_padding, _inputField.Bottom + _padding),
                BorderStyle = BorderStyle.FixedSingle,
                Name = "ResultLabel",
                Size = new Size(_defaultWidth - _padding * 2, _resultLabelHeight),
                Font = new Font("Arial", 20),
                TabIndex = 1
            };
            _resultLabel.MinimumSize = _resultLabel.Size;
            Controls.Add(_resultLabel);

            // Buttons
            var grid = GetButtonGrid();
            var rows = grid.GetLength(0);
            var columns = grid.GetLength(1);
            _buttons = new Button[rows, columns];
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    _buttons[i, j] = CreateButton(grid[i, j], i*columns+j+2);
                }
            }

            OnResize();

            ResumeLayout(false);
            PerformLayout();
        }

        private void OnResize()
        {
            _inputField.Width = ClientSize.Width - _padding * 2;
            _resultLabel.Width = ClientSize.Width - _padding * 2;

            var rows = _buttons.GetLength(0);
            var columns = _buttons.GetLength(1);
            int width = (ClientSize.Width - _padding * (columns + 1)) / columns;
            int height = (ClientSize.Height - _resultLabel.Bottom - _padding * (rows + 1)) / rows;
            var size = new Size(width, height);
            var offset = new Size(_padding, _resultLabel.Bottom + _padding);
            var ceil = size + new Size(_padding, _padding);
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    _buttons[i, j].Location = new Point(ceil.Width * j, ceil.Height * i) + offset;
                    _buttons[i, j].Size = size;
                }
            }
        }

        private void OnResize(object sender, EventArgs args)
        {
            OnResize();
        }

        private static Grammar GetGrammar()
        {
            var grammar = new Grammar();

            grammar.AddConstant("e", Math.E);
            grammar.AddConstant("pi", Math.PI);

            grammar.AddPrefixOp("-", a => -a);
            grammar.AddPrefixOp("sin", a => Math.Sin(a));
            grammar.AddPrefixOp("cos", a => Math.Cos(a));
            grammar.AddPrefixOp("ln", a => Math.Log(a));
            grammar.AddPrefixOp("exp", a => Math.Exp(a));
            grammar.AddPrefixOp("floor", a => Math.Floor(a));
            grammar.AddPrefixOp("ceil", a => Math.Ceiling(a));
            grammar.AddPrefixOp("round", a => Math.Round(a));
            grammar.AddPrefixOp("abs", a => Math.Abs(a));
            grammar.AddPrefixOp("log2", a => Math.Log2(a));
            grammar.AddPrefixOp("log10", a => Math.Log10(a));
            grammar.AddPrefixOp("sqrt", a => Math.Sqrt(a));

            grammar.AddBinaryOp("+", (a, b) => a + b, 1);
            grammar.AddBinaryOp("-", (a, b) => a - b, 1);
            grammar.AddBinaryOp("*", (a, b) => a * b, 2);
            grammar.AddBinaryOp("/", (a, b) => a / b, 2);
            grammar.AddBinaryOp("%", (a, b) => a % b, 2);
            grammar.AddBinaryOp("^", (a, b) => Math.Pow(a, b), 3);

            grammar.AddPostfixOp("!", a => Enumerable.Range(1, Convert.ToInt32(a)).Aggregate(1, (acc, b) => acc * b));

            return grammar;
        }

        private void Eval()
        {
            try
            {
                var function = _compiler.Compile(_inputField.Text + ' ');
                _resultLabel.Text = function.Evaluate().ToString();
            }
            catch(Exception e)
            {
                _resultLabel.Text = e.Message;
            }
        }

        private static string[,] GetButtonGrid()
        {
            return new string[,]
            {
                { "pi",     "e",      "exp ",  "CE",   "<-" },
                { "floor ", "round ", "ceil ", "abs ", "^" },
                { "sin",    "!",      "(",     ")",    "%" },
                { "cos ",   "7",      "8",     "9",    "/" },
                { "ln ",    "4",      "5",     "6",    "*" },
                { "log2 ",  "1",      "2",     "3",    "-" },
                { "log10 ", ".",      "0",     "=",    "+" }
            };
        }

        private EventHandler OnClickFactory(string label)
        {
            return label switch
            {
                "CE" => (_, _) =>
                {
                    _inputField.Clear();
                    _resultLabel.Text = String.Empty;
                },
                "<-" => (_, _) => _inputField.Text = _inputField.Text[..^1],
                "=" => (_, _) => Eval(),
                _ => (_, _) => _inputField.Text += label
            };
        }

        private Button CreateButton(String label, int tabIndex)
        {
            var b = new Button
            {
                Text = label,
                UseVisualStyleBackColor = true,
                TextAlign = ContentAlignment.MiddleCenter,
                Parent = this,
                TabIndex = tabIndex
            };
            b.Click += OnClickFactory(label);
            b.Show();

            return b;
        }
    }
}