﻿using static System.String;
using System.Globalization;

namespace Functions;

public class Compiler
{
    private Grammar _grammar;

    public Compiler(Grammar grammar)
    {
        _grammar = grammar;
    }

    public Function Compile(string infix)
    {
        const TokenType operands = TokenType.Number | TokenType.Argument | TokenType.Prefix | TokenType.Open;
        const TokenType operators = TokenType.Binary | TokenType.Postfix | TokenType.Close;

        TokenType next = operands;
        var expression = new List<Token>();
        var stack = new Stack<Token>();

        int length;
        var i = 0;

        while (infix[i] == ' ')
            ++i;

        while (i < infix.Length)
        {
            if (next.HasFlag(TokenType.Number) && Grammar.MatchNumber(infix, i, out length))
            {
                expression.Add(new Token(TokenType.Number, infix.Substring(i, length)));
                next = operators;
            }
            else if (next.HasFlag(TokenType.Prefix) && _grammar.MatchPrefix(infix, i, out length))
            {
                stack.Push(new Token(TokenType.Prefix, infix.Substring(i, length)));
                next = operands;
            }
            else if (next.HasFlag(TokenType.Binary) && _grammar.MatchBinary(infix, i, out length))
            {
                string signature = infix.Substring(i, length);
                int precedence = _grammar.Precedence(signature);
                while (stack.Count > 0
                    && (stack.Peek().Type == TokenType.Prefix || _grammar.Precedence(stack.Peek().Value) > precedence))
                    expression.Add(stack.Pop());
                stack.Push(new Token(TokenType.Binary, signature));
                next = operands;
            }
            else if (next.HasFlag(TokenType.Postfix) && _grammar.MatchPostfix(infix, i, out length))
            {
                expression.Add(new Token(TokenType.Postfix, infix.Substring(i, length)));
                next = operators;
            }
            else if (next.HasFlag(TokenType.Open) && infix[i] == '(')
            {
                stack.Push(new Token(TokenType.Open, Empty));
                next = operands;
                length = 1;
            }
            else if (next.HasFlag(TokenType.Close) && infix[i] == ')')
            {
                while (stack.Peek().Type != TokenType.Open)
                    expression.Add(stack.Pop());
                stack.Pop();
                next = operators;
                length = 1;
            }
            else if (next.HasFlag(TokenType.Argument) && Grammar.MatchArgument(infix, i, out length))
            {
                expression.Add(new Token(TokenType.Argument, infix.Substring(i, length)));
                next = operators;
            }
            else
            {
                throw new InvalidOperationException($"Unexpected token at {i}; UnitGroup: {next}");
            }
            i += length;
            while (i < infix.Length && infix[i] == ' ')
                ++i;
        }

        while (stack.Count > 0)
            expression.Add(stack.Pop());

        var args = new HashSet<string>(expression.Where(t => t.Type == TokenType.Argument).Select(t => t.Value));
        return new Function(Compile(expression), args, infix, Join(' ', expression.Select(t => t.Value)));
    }

    private List<Function.Unit> Compile(List<Token> tokens)
    {
        return tokens.Select(token => token.Type switch
        {
            TokenType.Number => CompileNumber(token.Value),
            TokenType.Argument => CompileArgument(token.Value),
            TokenType.Prefix => CompilePrefix(token.Value),
            TokenType.Binary => CompileBinary(token.Value),
            TokenType.Postfix => CompilePostfix(token.Value),
            _ => throw new ArgumentException($"Unexpected TokenType: {token.Type}",
                                             nameof(tokens))
        })
                     .ToList();
    }

    private static Function.Unit CompileNumber(string s)
    {
        var x = double.Parse(s, NumberStyles.Number, CultureInfo.InvariantCulture);
        return (_, _) => x;
    }

    private Function.Unit CompileArgument(string s)
    {
        if (_grammar.Constants.TryGetValue(s, out double value))
            return (_, _) => value;
        return (_, argHook) => argHook(s);
    }

    private Function.Unit CompilePrefix(string s)
    {
        return CompileUnary(s, _grammar.PrefixOps);
    }

    private Function.Unit CompileBinary(string s)
    {
        if (_grammar.BinaryOps.TryGetValue(s, out Grammar.BinaryOperation? bop) == false)
            throw new ArgumentException($"Unknown binary operation: '{s}'", nameof(s));
        Grammar.Binary op = bop?.Operation ?? throw new InvalidOperationException();
        return (stack, _) =>
        {
            double a = stack.Pop();
            double b = stack.Pop();
            return op(b, a);
        };
    }

    private Function.Unit CompilePostfix(string s)
    {
        return CompileUnary(s, _grammar.PostfixOps);
    }

    private static Function.Unit CompileUnary(string s, IReadOnlyDictionary<string, Grammar.Unary> registry)
    {
        Grammar.Unary unary = registry[s];
        return (stack, _) => unary(stack.Pop());
    }
}
