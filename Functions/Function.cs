﻿namespace Functions;

public class Function
{
    public string Infix { get; }
    public string Postfix { get; }
    public IReadOnlySet<string> Args;

    public delegate double Unit(Stack<double> stack, Func<string, double> argHook);

    private List<Unit> _expression;

    public Function(List<Unit> expression,
                    IReadOnlySet<string> args,
                    string infix,
                    string postfix)
    {
        _expression = expression;
        Args = args;
        Infix = infix;
        Postfix = postfix;
    }

    public double Evaluate()
    {
        return Evaluate(name => throw new KeyNotFoundException(name));
    }

    public double Evaluate(Func<string, double> argHook)
    {
        var stack = new Stack<double>();
        foreach (Unit unit in _expression)
            stack.Push(unit(stack, argHook));
        return stack.Peek();
    }
}
