﻿using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Functions;

public class Grammar
{
    public delegate double Unary(double a);

    public delegate double Binary(double a, double b);

    internal record BinaryOperation(Binary Operation, int Precedence);

    private Dictionary<string, double> _constants = new();
    private Dictionary<string, Unary> _prefixOps = new();
    private Dictionary<string, BinaryOperation?> _binaryOps = new();
    private Dictionary<string, Unary> _postfixOps = new();

    internal IReadOnlyDictionary<string, double> Constants => _constants;
    internal IReadOnlyDictionary<string, Unary> PrefixOps => _prefixOps;
    internal IReadOnlyDictionary<string, BinaryOperation?> BinaryOps => _binaryOps;
    internal IReadOnlyDictionary<string, Unary> PostfixOps => _postfixOps;

    public Grammar AddConstant(string name, double value)
    {
        _constants.Add(name, value);
        return this;
    }

    public Grammar AddPrefixOp(string signature, Unary op)
    {
        _prefixOps.Add(signature, op);
        return this;
    }

    public Grammar AddBinaryOp(string signature, Binary op, int precedence)
    {
        _binaryOps.Add(signature, new BinaryOperation(op, precedence));
        return this;
    }

    public Grammar AddPostfixOp(string signature, Unary op)
    {
        _postfixOps.Add(signature, op);
        return this;
    }

    public static bool MatchNumber(string s, int start, out int length)
    {
        int signed = s[start] == '-' || s[start] == '+' ? 1 : 0;
        length = signed;
        while (char.IsDigit(s[start + length]))
            ++length;
        length += s[start + length] == '.' ? 1 : 0;
        while (char.IsDigit(s[start + length]))
            ++length;
        return length > signed;
    }

    public static bool MatchArgument(string s, int start, out int length)
    {
        length = 0;
        while (char.IsLetter(s[start + length]) || s[start + length] == '_')
            ++length;
        return length > 0;
    }

    public bool MatchPrefix(string s, int start, out int length)
    {
        return Match(s, start, _prefixOps, out length);
    }

    public bool MatchBinary(string s, int start, out int length)
    {
        return Match(s, start, _binaryOps, out length);
    }

    public bool MatchPostfix(string s, int start, out int length)
    {
        return Match(s, start, _postfixOps, out length);
    }

    public int Precedence(string op)
    {
        return _binaryOps.TryGetValue(op, out BinaryOperation? binary) ? (binary?.Precedence ?? 0) : 0;
    }

    private static bool Match<TOp>(string s, int start, Dictionary<string, TOp> ops, out int length)
    {
        var sub = s[start..];
        foreach (KeyValuePair<string, TOp> pair in ops)
        {
            if (sub.StartsWith(pair.Key))
            {
                length = pair.Key.Length;
                return true;
            }
        }
        length = 0;
        return false;
    }
}
