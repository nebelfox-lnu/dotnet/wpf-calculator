﻿namespace Functions;

public record Token(TokenType Type, string Value);
