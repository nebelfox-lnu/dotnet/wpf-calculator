﻿namespace Functions;

[Flags]
public enum TokenType
{
    Number = 0,
    Prefix = 1,
    Binary = 2,
    Postfix = 4,
    Open = 8,
    Close = 16,
    Argument = 32
}
