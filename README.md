# Calculator

Frontend is powered by WinForms. 
Size of all widgets scales dynamically filling all the space on window resize.
Input field and result label are created via Form Editor.
All the buttons are generated dynamically from code.

Backend is powered by custom library `Functions` for evaluating arithmetical expressions with support for custom arguments, constants, prefix and binary and postfix operators. 
(It's a C# port of the respective [C++ Library](https://github.com/NebelFox/Functions))
